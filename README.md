# BVS Courtage

## Description
Le projet est une mise en forme d'un fichier excel fournie par la société afin de leur fournir une manière plus ludique pour entrer les données de leurs clients, récuperer les fichiers qu'ils ont fournis, et les faire signer en ligne des pdf générés par l'application. 
il est utilisable sur desktop et sur mobile.


## Statut du projet
Il a été conçu pendant un sprint de la promotion Simplon Narbonne #6, de ce fait il n'est pas totalement aboutit.

## Environnement requis
* LAMP 
* PHP 8
* MySql 8.0.27

## Installation
* Installer LAMP
* cloner le dépot GIT dans la dossier racine de la machine.
[depot Git](https://gitlab.com/caulet.guillaume.x/bvs-courtage)
* Il faut init le fichier init.sql dans le terminal avec 

```
sudo mysql > ../model/init.sql
```
* Dans le fichier data.php
Il faut changer les codes de connexion à la base de donnée

```php
$user = "moi";
$pass ="test";
```
le point d'entrée de l'app : /view/index.php

