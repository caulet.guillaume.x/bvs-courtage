<?php
session_start(); 

include "../model/data.php";


if(  
    isset($_POST['nom_Enfant']) &&
    isset($_POST['prenom_Enfant']) && 
    isset($_POST['date_Naissance_Enfant']) &&
    isset($_POST['N_SS_Enfant']) &&
    isset($_POST['sexe_Enfant']) && 
    $_POST['nom_Enfant'] !== "" &&
    $_POST['prenom_Enfant'] !== "" && 
    $_POST['date_Naissance_Enfant'] !== "" &&
    $_POST['N_SS_Enfant'] !== "" &&
    $_POST['sexe_Enfant'] !== ""
    ){
$nom_Enfant = $_POST['nom_Enfant'];
$prenom_Enfant = $_POST['prenom_Enfant']; 
$dateNaissance_Enfant = $_POST['date_Naissance_Enfant']; 
$nSS_Enfant = $_POST['N_SS_Enfant'];
$sexe_Enfant = $_POST['sexe_Enfant'];
$idE = ajouterEnfant($nom_Enfant,$prenom_Enfant,$dateNaissance_Enfant,$nSS_Enfant,$sexe_Enfant);
$idP = $_SESSION['compte']['id du parent'];
}
if(isset($idE) && isset($idP)){lierP_E($idP,$idE);}
$nbr = $_SESSION['compte']['nbr'];
$idP = $_SESSION['compte']['id du parent'];
$nbr -= 1;
$_SESSION['compte'] = ["id du parent" => $idP, "nbr" => $nbr];

if($_SESSION['compte']['nbr'] !== 0){
    header('location:../view/formEnfant.php');
}else if($_SESSION['compte']['nbr'] < 1){
    header('location:../view/listeAssuré.php');
    session_destroy();
}

