<?php include '../model/data.php';
 $assure = voir1Assure($_GET['id']);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="../documents/favicon.png" />
    <title>Contrats de l'entreprise</title>
</head>
<header>
    <?php include 'header.php' ?>
</header>
<body>
        <div class="container titre-de-la-page">
            <h2>Contrats de l'assuré</h2>
            <div class="grid container">
                <div class="row titre" href="fiche-entreprise.php">
                    <div class="col-md-6">assuré n°<?php 
                    echo $assure[0]['id']?></div>
                    <div class="col-md-6"><?php echo $assure[0]['nom']; echo $assure[0]['prenom']?></div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">Informations</div>
                    <div class="col-md-6">Contrats</div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <p>Date de naissance: <?php echo $assure[0]['date_De_Naissance']?> à <?php echo $assure[0]['lieu_De_Naissance']?></p>
                        <p>Sexe: <?php echo boolSexe($assure[0]['sexe']); ?></p>
                        <p>N°Sécurité Sociale: <?php echo $assure[0]['nSS']?></p>
                        <p>Caisse Maladie ou Retraite: <?php echo $assure[0]['caisse_Mal_Re']?></p>
                        <p>Adresse: <?php echo $assure[0]['adresse']?></p>
                        <p>Numéro de tel: <?php echo $assure[0]['mobile']?></p>
                        <p>Numéro de tel bureau: <?php echo $assure[0]['tel_Bureau']?></p>
                        <p>Mail: <?php echo $assure[0]['mail']?></p>
                        <p>Statut: <?php echo $assure[0]['statut']?></p>
                        <p>Chef d'entreprise: <?php echo $assure[0]['chef_Entreprise']?></p>
                        <p>Profession: <?php echo $assure[0]['profession']?></p>
                        <p>Situation Familiale: <?php echo $assure[0]['situation_Familiale']?> </p>
                        <p>Parent: <?php echo boolParent($assure[0]['parent']); ?></p>
                        <p>Nombre d'enfant: <?php echo $assure[0]['nb_Enfant']?> <p>
                        <p>Fumeur: <?php echo boolFumeur($assure[0]['fumeur']); ?><p>
                        <button class="btn" type="submit">Modifier</button>
                    </div>
                    <div class="col-md-6">
                        <a class="recueil" href="recueil.php">Recueil n°1</a>
                        <a class="recueil" href="recueil.php">Recueil n°2</a>
                        <p class="recueil_supp">Recueil n°3 (Annulé)</p>
                        <a href="liste-recueil.php"><button class="btn" type="submit">Ajouter</button></a>
                    </div>
                </div>
            
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</html>