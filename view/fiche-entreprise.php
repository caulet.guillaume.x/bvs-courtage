<?php
session_start();
 include '../model/data.php';
 $entreprise = voir1Entreprise($_GET['id']);
 $_SESSION['idE'] = $entreprise[0]['id'];
 $particulier = voirTBlaison($_GET['id_Assure']);
 var_dump($_GET['id_Assure']);
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="../documents/favicon.png" />
    <title>Fiche entreprise</title>
</head>
<header>
    <?php include 'header.php' ?>
</header>
<body>
    <div class="container">
        <div class="grid container">
            <div class="row titre" href="fiche-entreprise.php">
                <div class="col-md-6">assuré n°<?php 
                    echo $entreprise[0]['id']?></div>
                <div class="col-md-6"><?php 
                    echo $entreprise[0]['raison_Sociale']?></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">Info Entreprise</div>
                <div class="col-md-6">Chef d'Entreprise</div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <p>Numéro de siret: <?php echo $entreprise[0]['siret']?></p>
                    <p>Adresse: <?php echo $entreprise[0]['adresse']?></p>
                    <p>Effectif: <?php echo $entreprise[0]['effectif']?></p>
                    <p>Activité: <?php echo $entreprise[0]['activite']?></p>
                    <p>Forme Juridique: <?php echo $entreprise[0]['forme_Juridique']?></p>
                    <p>Date de Création:<?php echo $entreprise[0]['date_De_Creation']?></p>
                    <p>Chiffre d'Affaire: <?php echo $entreprise[0]['chiffre_Affaire']?> €</p>
                    <p>Code NAF: <?php echo $entreprise[0]['code_naf']?></p>
                    <button class="btn" type="submit">Modifier</button>
                </div>
                <div class="col-md-6">
                    <p>Nom:</p>
                    <p>Prénom:</p>
                    <p>Mail:</p>
                    <p>Numéro de tel:</p>
                    <button class="btn" type="submit">Modifier</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-md-12 client-assuré-entreprise">
                    <h2>Les clients assurés de l'entreprise
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">Prénom</div>
                <div class="col-md-4">Nom</div>
                <div class="col-md-4">Id</div>
            </div>
            <?php foreach(Voir1Assure($particulier[0]['id_Assure']) as $select){ ?>
            <div class="row">
                <div class="col-md-4"><?php echo $select['prenom']?></div>
                <div class="col-md-4"><?php echo $select['nom'] ?></div>
                <div class="col-md-4"><?php echo $select['id'] ?></div>
            </div>
            <?php } ?>
            <hr>
            <form action="../control/controlE_A.php" method="post">
                <input type="number" name="idA">
                <input type="submit" value="Ajouter un employé à l'entreprise">
            </form>
            <hr>
            <a href="contrat-entreprise.php"><button class="btn" type="submit">Voir les contrats</button></a>
        </div>
    </div>