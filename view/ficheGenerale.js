var compteurEnfant = 0;



// Afficher, cacher les carousels quand on change de menu
var boutonAssuré = document.getElementById("boutonAssuré");
var boutonAssuré2 = document.getElementById("boutonAssuré2");
var boutonEnfant = document.getElementById("boutonEnfant");
var boutonEntreprise = document.getElementById("boutonEntreprise");


var carouselAssuré = document.getElementById('carouselAssuré');
var carouselAssuré2 = document.getElementById('carouselAssuré2');
var carouselEnfant = document.getElementById("carouselEnfant")
var carouselEntreprise = document.getElementById("carouselEntreprise")





boutonAssuré.addEventListener("click", assuréShowCarousel);
function assuréShowCarousel ()
{
    carouselAssuré.classList.remove('ligneCaché');
    carouselAssuré2.classList.add('ligneCaché');
    carouselEnfant.classList.add('ligneCaché');
    carouselEntreprise.classList.add('ligneCaché');
}


boutonAssuré2.addEventListener("click", assuré2ShowCarousel);
function assuré2ShowCarousel ()
{
    carouselAssuré.classList.add('ligneCaché');
    carouselAssuré2.classList.remove('ligneCaché');
    carouselEnfant.classList.add('ligneCaché');
    carouselEntreprise.classList.add('ligneCaché');
}


boutonEnfant.addEventListener("click", enfantShowCarousel);
function enfantShowCarousel ()
{
    carouselAssuré.classList.add('ligneCaché');
    carouselAssuré2.classList.add('ligneCaché');
    carouselEnfant.classList.remove('ligneCaché');
    carouselEntreprise.classList.add('ligneCaché');
}


boutonEntreprise.addEventListener("click", entrepriseShowCarousel);
function entrepriseShowCarousel ()
{
    carouselAssuré.classList.add('ligneCaché');
    carouselAssuré2.classList.add('ligneCaché');
    carouselEnfant.classList.add('ligneCaché');
    carouselEntreprise.classList.remove('ligneCaché');
}












// Recapitulatif infos generales du particulier
var jsonAssuré = {};
var listeInfos = document.getElementById("listeInfos");

var recapInputs = function (){

    while (listeInfos.firstChild) {
        listeInfos.firstChild.remove()
    }

    jsonAssuré = {};
    var inputs = document.getElementsByClassName("entrée")
    var ul = document.createElement("ul");

    for (entrée of inputs) { 

        // Stockage de la valeur pour l'envoi
        jsonAssuré[entrée.name] = entrée.value; 

        // Affichage titre de l'info
        var li = document.createElement("li");
        var name = document.createElement("h3");
        name.innerText = entrée.id;
        li.appendChild(name);

        //Affichage valeur de l'info
        var value = document.createElement("div");
        if (entrée.value != ""){
            value.innerText = entrée.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }


        ul.appendChild(li);

    }

    listeInfos.appendChild(ul);

}


// Update du tableau infos generales dés qu'on clique sur une fleche du carousel
boutonsUpdate = document.getElementsByClassName("updateRecapitulatif");
for (button of boutonsUpdate){
    button.addEventListener("click", recapInputs );
}








// Assuré 2

var jsonAssuré2 = {};
var listeInfosAssuré2 = document.getElementById("listeInfosAssuré2");

var recapInputsAssuré2 = function (){

    while (listeInfosAssuré2.firstChild) {
        listeInfosAssuré2.firstChild.remove()
    }

    jsonAssuré2 = {};
    var inputs = document.getElementsByClassName("entréeAssuré2")
    var ul = document.createElement("ul");

    for (entréeAssuré2 of inputs) { 

        // Stockage pour l'envoi
        jsonAssuré2[entréeAssuré2.id] = entréeAssuré2.value; 

        // Affichage titre
        var li = document.createElement("li");
        var name = document.createElement("h3");
        name.innerText = entréeAssuré2.id;
        li.appendChild(name);

        // Affichage valeur
        var value = document.createElement("div");
        if (entréeAssuré2.value != ""){
            value.innerText = entréeAssuré2.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }

        ul.appendChild(li);

    }

    listeInfosAssuré2.appendChild(ul);

}


boutonsUpdate = document.getElementsByClassName("updateRecapitulatifAssuré2");
for (button of boutonsUpdate){
    button.addEventListener("click", recapInputsAssuré2 );
}









// enfants
//infosEnfants.children.length


// variables
var listeEnfants = [];
var choisirEnfantAffiché = document.getElementById("choisirEnfantAffiché");
var addEnfant = document.getElementById('addEnfant');
var infosEnfants = document.getElementById("infosEnfants");


// bouton add
addEnfant.addEventListener("click", ajouterEnfant );
function ajouterEnfant  ()
{

    var inputs = document.getElementsByClassName("infosEnfant")
    var listeInputs = {};
    var ul = document.createElement("ul");

    // Pour chaque inputs du formulaire enfant
    for (entrée of inputs) { 

        // on la rajoute dans le tableau
        listeInputs[entrée.id] = entrée.value;
        
        // Affichage titre info
        var li = document.createElement("li");
        var name = document.createElement("h3");
        name.innerText = entrée.id;
        // .replaceAll("_", " ")
        li.appendChild(name);

        //Affichage valeur info
        var value = document.createElement("div");

        if (entrée.value != ""){
            value.innerText = entrée.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }

        // Id de l'affichage de cet enfant (pour le modifier ou le supprimer plus tard)
        ul.id = "enfant " + ( compteurEnfant) ;
        ul.appendChild(li);
    }

    // cacher l'enfant qu'on vient d'ajouter
    ul.classList.add('ligneCaché');


    // Numero de l'enfant pour le retrouver dans le tableau de données (pour le modifier ou le supprimer plus tard)
    listeInputs["identifiantFront"] = compteurEnfant;
    listeEnfants.push(listeInputs);
    infosEnfants.appendChild(ul);



    // creation d'un bouton pour afficher l'enfant créer
    var buttonAfficher = document.createElement("button"); 
    buttonAfficher.innerHTML = "Afficher enfant " + ( compteurEnfant + 1) ;
    buttonAfficher.id = "afficher enfant " + ( compteurEnfant) ;
    buttonAfficher.onclick = function(evt)
{

    for(element of infosEnfants.children){
       //console.log(evt);
        if ( element.id == evt.target.id.slice(9)){
            element.classList.remove('ligneCaché');
        }
        else {
            element.classList.add('ligneCaché');
        }
    }
}



   // creation d'un bouton pour supprimer l'enfant créer
   var buttonSupprimer = document.createElement("button"); 
   buttonSupprimer.innerHTML = "Supprimer enfant " + ( compteurEnfant + 1) ;
   buttonSupprimer.id = "supprimer enfant " + ( compteurEnfant) ;
   buttonSupprimer.onclick = function(evt)
{

   for(element of infosEnfants.children){
      //console.log(evt);
       if ( element.id == evt.target.id.slice(10)){

        for(ligne of listeEnfants){
            console.log(ligne["identifiantFront"]);
            console.log(evt.target.id.slice(17));
            console.log(listeEnfants.indexOf(ligne));
            if (ligne["identifiantFront"] == evt.target.id.slice(17)){
                listeEnfants.splice(listeEnfants.indexOf(ligne), 1);
            }
        }
    
           element.remove();
           evt.target.remove();
           document.getElementById("afficher " + element.id).remove();
       }
       else {
          
       }
   }
}


choisirEnfantAffiché.appendChild(buttonAfficher);
choisirEnfantAffiché.appendChild(buttonSupprimer);
compteurEnfant++;
console.log(listeEnfants);
}   









// Bouton formulaire modif infos enfants
var updateEnfant = document.getElementById("updateEnfant");
updateEnfant.addEventListener("click", modifEnfant );

function modifEnfant  ()
{
    var numéro = document.getElementById("inputNuméro");
    var trucAModif = document.getElementById("trucAModif")
    var valueModif = document.getElementById("valueModif")


    // On cherche le bon enfant puis on modifie sa valeur dans le tableau de données
    for(ligne of listeEnfants){
        if (ligne["identifiantFront"] == (numéro.value - 1)){
            ligne[trucAModif.value] = valueModif.value;
        }
    }



    // On cherche le bon enfant puis on modifie sa valeur dans l'affichage
    for(ul of infosEnfants.children){
         if ( ul.id == "enfant " + ( numéro.value - 1) ){
             for (li of ul.children) {
                 if (li.children[0].innerHTML == trucAModif.value)
                 {
                    li.children[1].innerHTML = valueModif.value;
                    li.children[1].className = "";
                 }
             }
         }

    console.log(listeEnfants);
}
}








// Recapitulatif infos generales du particulier
var jsonEntreprise = {};
var listeInfosEntreprise = document.getElementById("listeInfosEntreprise");

var recapInputsEntreprise = function (){

    while (listeInfosEntreprise.firstChild) {
        listeInfosEntreprise.firstChild.remove()
    }


    jsonEntreprise = {};
    var inputs = document.getElementsByClassName("infosEntreprise")
    var ul = document.createElement("ul");

    for (entrée of inputs) { 

        // Tableau de donnée
        jsonEntreprise[entrée.id] = entrée.value; 

        // Affichage titre
        var li = document.createElement("li");
        var name = document.createElement("h3");
        name.innerText = entrée.id;
        li.appendChild(name);

        // Affichage info 
        var value = document.createElement("div");
        if (entrée.value != ""){
            value.innerText = entrée.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }

        ul.appendChild(li);
    }
    listeInfosEntreprise.appendChild(ul);
}


// Update du tableau infos generales dés qu'on clique sur une fleche du carousel
boutonsUpdateEntreprise = document.getElementsByClassName("updateRecapitulatifEntreprise");
for (button of boutonsUpdateEntreprise){
    button.addEventListener("click", recapInputsEntreprise );
}






// Envoi du formulaire


document.getElementById("testjson").addEventListener("click", async function () {

    let data = [];
    data.push(jsonAssuré);
    data.push(jsonAssuré2)
    data.push(listeEnfants);
    data.push(jsonEntreprise);


    let sendresponseAssuré = await fetch('../control/controlAssuréMobile.php', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(r => {
        if (r.code = 200){
            console.log("ok");
            //Redirection
            window.location = "listeAssuré.php";

        }

        else {
            window.alert("Problème lors de l'envoi, verifiez les données que vous envoyez");
        }
    })
    ;

});