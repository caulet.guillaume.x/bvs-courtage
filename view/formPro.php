<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../formPC.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="../documents/favicon.png" />
    <link rel="stylesheet" href="styles.css">
    <title>Formulaire Pro</title>
</head>
<header>
    <?php include 'header.php'; ?>
</header>

<body>
    <h2>FORMULAIRE PROFESSIONEL</h2>
    <div class="formulaire-general-Pc">
        <form action="../control/controlPro.php" method="post" enctype="multipart/form-data">
            <div class="row">
                <!-- ligne 1 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>Raison Sociale</label>
                    <input class="form-control" name="raison_Sociale" type="text" placeholder="Raison Sociale" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>Adresse</label>
                    <input class="form-control" name="adresse" type="text" placeholder="Adresse" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 2 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>Date de Création</label>
                    <input class="form-control" name="date_De_Creation" type="date" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>SIRET</label>
                    <input class="form-control" name="siret" type="number" autocomplete="off">
                </div>
                <!-- pattern="[0-9]{3}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{3}[ \.\-]?[0-9]{5}" -->
            </div>
            <div class="row">
                <!-- ligne 3 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>Effectif</label>
                    <input class="form-control" name="effectif" type="number" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>Code NAF</label>
                    <input class="form-control" name="code_naf" type="text" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 4 -->
                <div class="col">
                    <!-- colonne 2 -->
                    <label>Chiffre d'affaire</label>
                    <input class="form-control" name="chiffre_Affaire" type="number" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 5 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>Activité</label>
                    <input class="form-control" name="activite" type="text" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>Forme Juridique</label>
                    <input class="form-control" name="forme_Juridique" type="text" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 6 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>CCN</label>
                    <input class="form-control" name="ccn" type="text" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>K-BIS</label>
                    <input class="form-control" name="kbis" type="file" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 7 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>CNI Recto</label>
                    <input class="form-control" name="cni_Recto" type="file" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>CNI Verso</label>
                    <input class="form-control" name="cni_Verso" type="file" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 8 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>RIB PRO</label>
                    <input class="form-control" name="rib_pro" type="file" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>BILAN N-1</label>
                    <input class="form-control" name="bilan_N1" type="file" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 9 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>CARTES GRISES</label>
                    <input class="form-control" name="carte_Grise" type="file" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>BAIL (si locataire)</label>
                    <input class="form-control" name="bail" type="file" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 10 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>Q4</label>
                    <input class="form-control" name="q4" type="file" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>Q18</label>
                    <input class="form-control" name="q18" type="file" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 11 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>Q19</label>
                    <input class="form-control" name="q19" type="file" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <label>CONDITIONS PARTICULIÈRES</label>
                    <input class="form-control" name="condition_Particuliere" type="file" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <!-- ligne 12 -->
                <div class="col">
                    <!-- colonne 1 -->
                    <label>ATTESTATION SECURITE SOCIALE DE TOUTES LES PERSONNES</label>
                    <input class="form-control" name="attestation_SS" type="file" autocomplete="off">
                </div>
                <div class="col">
                    <!-- colonne 2 -->
                    <a href="#"><button class="btn" type="submit">Envoyer</button></a>
                </div>
            </div>
        </form>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

</html>