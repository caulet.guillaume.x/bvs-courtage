<?php include '../model/data.php'; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="../documents/favicon.png" />
    <title>Liste des entreprises assurées</title>
</head>
<header>
    <?php include 'header.php' ?>
</header>
<body>
  <div class="liste">
    <a href="formPro.php" class="btn ">Formulaire</a>
    <div class="tableaux">
      <table class="table table-hover table-striped table-bordered">
<h3>Liste des Entreprises</h3>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Raison sociale</th>
            <th scope="col">Siret</th>
            <th scope="col">Code NAF</th>
            <th scope="col">Activité</th>
            <th scope="col">Forme Juridique</th>
          </tr>
        </thead>
        <?php $arpr = 1 ?>
        <tbody>
          <?php foreach (VoirEntreprise() as $select) { ?>
            <tr>
              <th scope="row" class="th-tbody"><?php echo $select['id']; ?></th>
              <td> <a class="entreprise entreprise<?= $arpr++ ?>" href="fiche-entreprise.php?id=<?php echo $select['id'] ?>"><?php echo $select['raison_Sociale']; ?></a></td>
              <td> <a class="entreprise entreprise<?= $arpr++ ?>" href="fiche-entreprise.php?id=<?php echo $select['id'] ?>"><?php echo $select['siret']; ?></a></td>
              <td> <a class="entreprise entreprise<?= $arpr++ ?>" href="fiche-entreprise.php?id=<?php echo $select['id'] ?>"><?php echo $select['code_naf']; ?></a></td>
              <td> <a class="entreprise entreprise<?= $arpr++ ?>" href="fiche-entreprise.php?id=<?php echo $select['id'] ?>"><?php echo $select['activite']; ?></a></td>
              <td> <a class="entreprise entreprise<?= $arpr++ ?>" href="fiche-entreprise.php?id=<?php echo $select['id'] ?>"><?php echo $select['forme_Juridique']; ?></a></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>