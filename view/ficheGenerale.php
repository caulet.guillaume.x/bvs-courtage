<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <link rel="icon" type="image/png" href="../documents/favicon.png" />
  <title>Fiche générale</title>
</head>
<header>
  <?php include 'header.php' ?>
</header>

<body>
  <div class="collapse" id="navbarToggleExternalContent">
    <div class="bg-dark p-4">
      <h5 class="text-white h4">Recueils</h5>
      <span class="text-muted"></span>
      <button type="button" class="btn btn-outline-light">MRP</button>
      <button type="button" class="btn btn-outline-light">Auto</button>
      <button type="button" class="btn btn-outline-light">MRH</button>
      <button type="button" class="btn btn-outline-light">Protection juridique Pro/Particulier</button>
      <button type="button" class="btn btn-outline-light">PER/Assurance Vie</button>
      <button type="button" class="btn btn-outline-light">Prévoyance</button>
      <button type="button" class="btn btn-outline-light">Mutuelle</button>
      <button type="button" class="btn btn-outline-light">Assurance de Prêt</button>
      <button type="button" class="btn btn-outline-light">KDC/dépendance/obsèque/garantie/associe/homme Clé/GAV</button>
    </div>
  </div>
  <nav class="navbar navbar-dark bg-dark text-light">
    <div class="container-fluid">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <button type="button" class="btn btn-outline-light" id="boutonAssuré">Assuré</button>
      <button type="button" class="btn btn-outline-light" id="boutonAssuré2">Assuré 2</button>
      <button type="button" class="btn btn-outline-light" id="boutonEnfant">Enfant(s)</button>
      <button type="button" class="btn btn-outline-light" id="boutonEntreprise">Entreprise</button>
    </div>
  </nav>
  <form action="../control/controlAssuréMobile.php" method="post" class="was-validated" enctype="multipart/form-data">
    <div class="container mt-3 col-sm-2 text-center  d-flex flex-column justify-content-center">
      <h1>Fiche Générale</h1>
    </div>
    <div id="carouselAssuré" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-inner" id="carouselAssuréInner">
        <h3>Assuré principal</h3>
        <div class="carousel-item active">
          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control entrée" id="nom" name="last-name" autocomplete="off" required>
            <label>Nom:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>
          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control entrée" id="prenom" name="first-name" autocomplete="off" required>
            <label>Prénom:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3">
            <label>Sexe:</label>
            <select id="Sexe" class="selectpicker form-select entrée" name="sexe" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option value="0">Homme</option>
              <option value="1">Femme</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control entrée" id="Adresse" name="adresse" autocomplete="off" required>
            <label>Adresse:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>
        </div>

        <div class="carousel-item">
          <div class="mb-3">
            <label>Fumeur:</label>
            <select id="Fumeur" class="selectpicker form-select entrée" name="fumeur" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option value="1">Oui</option>
              <option value="0">Non</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>

          <div class="mb-3">
            <label>Statut:</label>
            <select id="Statut" class="selectpicker form-select entrée" name="status" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option>Salarié</option>
              <option>Sans emploi</option>
              <option>Chef d'entreprise TNS</option>
              <option>Mandataire social</option>
              <option>Agriculteur/éleveur</option>
              <option>Retraité SS</option>
              <option>Retraité TNS</option>
              <option>Etudiant</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="mb-3">
            <label>Caisse maladie / Retraite :</label>
            <select id="Caisse maladie / Retraite" class="selectpicker form-select entrée" name="C-M/R" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option>SS</option>
              <option>SSI</option>
              <option>PL</option>
              <option>MSA</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="mb-3">
            <label>Situation familiale:</label>
            <select id="Situation familiale" class="selectpicker form-select entrée" name="S-F" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option>Célibataire</option>
              <option>Union libre / Concubin</option>
              <option>Marié(e)</option>
              <option>Divorcé(e)</option>
              <option>Séparé(e)</option>
              <option>Veuf</option>
              <option>Pacsé(e)</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>





        </div>




        <div class="carousel-item">
          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="mobile" name="mobile" autocomplete="off" required>
            <label>Mobile:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Tel Bureau" name="tel_bureau" autocomplete="off" required>
            <label>Tel Bureau:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="email" name="email" autocomplete="off" required>
            <label>Email</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="date" class="form-control entrée" id="Date de naissance" name="DN" autocomplete="off" required>
            <label>Date de naissance:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


        </div>


        <div class="carousel-item">
          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Lieu de naissance" name="LN" autocomplete="off" required>
            <label>Lieu de naissance:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Profession" name="profession" autocomplete="off" required>
            <label>Profession:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Numéro de securité sociale" name="N°SS" autocomplete="off" required>
            <label>Numéro de securité sociale:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3">
            <label>Si chef d'entreprise</label>
            <input class=" form-control entrée" list="browsers" id="Si chef d'entreprise" name="chef" required>
            <datalist id="browsers">
              <option value="Nom Propre">
              <option value="Gérant Majoritaire">
              <option value="Mandataire social">
              <option value="Auto entrepreneur">
              <option value="Profession libérale">
                <div class="valid-feedback">Rempli</div>
                <div class="invalid-feedback">Non rempli</div>
            </datalist>
          </div>
        </div>


        <div class="carousel-item">
          <div class="mb-3">
            <label>Parent:</label>
            <select id="Parent" class="selectpicker form-select entrée" name="parent" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option value="1">Oui</option>
              <option value="0">Non</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Nombre enfants" name="nb_Enfant" autocomplete="off" required>
            <label>Nombre enfants:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="cni recto" name="cni_Recto" autocomplete="off" required>
            <label>cni recto:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="cni verso" name="cni_Verso" autocomplete="off" required>
            <label>cni verso:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

        </div>


        <div class="carousel-item">

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Recto permis de conduire" name="permis_De_Conduire_Recto" autocomplete="off" required>
            <label>Recto permis de conduire:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Verso permis de conduire" name="permis_De_Conduire_Verso" autocomplete="off" required>
            <label>Verso permis de conduire:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="RIB perso" name="rib_perso" autocomplete="off" required>
            <label>RIB perso:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Tableau amortissement" name="tableau_amortissement" autocomplete="off" required>
            <label>Tableau amortissement:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

        </div>


        <div class="carousel-item">

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Carte grise" name="carte_Grise" autocomplete="off" required>
            <label>Carte grise:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Condition particulière" name="condition_Particuliere" autocomplete="off" required>
            <label>Condition Particuliere:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entrée" id="Attestion securité sociale" name="attestion_SS" autocomplete="off" required>
            <label>Attestion securité sociale:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

        </div>

        <div class="carousel-item">
          <h2>Recapitulatif</h2>
          <div id="listeInfos"> </div>


        </div>


      </div>
      <button class="carousel-control-prev border rouge updateRecapitulatif" type="button" data-bs-target="#carouselAssuré" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next border rouge updateRecapitulatif" type="button" data-bs-target="#carouselAssuré" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>

    </div>


    <!-- Assuré2 -->


    <div id="carouselAssuré2" class="carousel slide ligneCaché" data-bs-ride="carousel">
      <div class="carousel-inner" id="carouselAssuré2Inner">
        <h2>Assuré secondaire</h2>



        <div class="carousel-item active">

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control entréeAssuré2" id="nom" name="last-name" autocomplete="off" required>
            <label>Nom:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control entréeAssuré2" id="prenom" name="first-name" autocomplete="off" required>
            <label>Prénom:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3">
            <label>Sexe:</label>
            <select id="Sexe" class="selectpicker form-select entréeAssuré2" name="sexe" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option value="0">Homme</option>
              <option value="1">Femme</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control entréeAssuré2" id="Adresse" name="adresse" autocomplete="off" required>
            <label>Adresse:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>






        </div>
        <div class="carousel-item">
          <div class="mb-3">
            <label>Fumeur:</label>
            <select id="Fumeur" class="selectpicker form-select entréeAssuré2" name="fumeur" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option value="1">Oui</option>
              <option value="0">Non</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>

          <div class="mb-3">
            <label>Statut:</label>
            <select id="Statut" class="selectpicker form-select entréeAssuré2" name="status" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option>Salarié</option>
              <option>Sans emploi</option>
              <option>Chef d'entreprise TNS</option>
              <option>Mandataire social</option>
              <option>Agriculteur/éleveur</option>
              <option>Retraité SS</option>
              <option>Retraité TNS</option>
              <option>Etudiant</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="mb-3">
            <label>Caisse maladie / Retraite :</label>
            <select id="Caisse maladie / Retraite" class="selectpicker form-select entréeAssuré2" name="C-M/R" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option>SS</option>
              <option>SSI</option>
              <option>PL</option>
              <option>MSA</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="mb-3">
            <label>Situation familiale:</label>
            <select id="Situation familiale" class="selectpicker form-select entréeAssuré2" name="S-F" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option>Célibataire</option>
              <option>Union libre / Concubin</option>
              <option>Marié(e)</option>
              <option>Divorcé(e)</option>
              <option>Séparé(e)</option>
              <option>Veuf</option>
              <option>Pacsé(e)</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>





        </div>




        <div class="carousel-item">
          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="mobile" name="mobile" autocomplete="off" required>
            <label>Mobile:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="Tel Bureau" name="tel_bureau" autocomplete="off" required>
            <label>Tel Bureau:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="email" name="email" autocomplete="off" required>
            <label>Email</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="Date de naissance" name="DN" autocomplete="off" required>
            <label>Date de naissance:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


        </div>


        <div class="carousel-item">
          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="Lieu de naissance" name="LN" autocomplete="off" required>
            <label>Lieu de naissance:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="Profession" name="profession" autocomplete="off" required>
            <label>Profession:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="Numéro de securité sociale" name="N°SS" autocomplete="off" required>
            <label>Numéro de securité sociale:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3">
            <label>Si chef d'entreprise</label>
            <input class=" form-control entréeAssuré2" list="browsers" id="Si chef d'entreprise" name="chef" required>
            <datalist id="browsers">
              <option value="Nom Propre">
              <option value="Gérant Majoritaire">
              <option value="Mandataire social">
              <option value="Auto entrepreneur">
              <option value="Profession libérale">
                <div class="valid-feedback">Rempli</div>
                <div class="invalid-feedback">Non rempli</div>
            </datalist>
          </div>
        </div>

        <div class="carousel-item">
          <div class="mb-3">
            <label>Parent:</label>
            <select id="parent" class="selectpicker form-select entréeAssuré2" name="parent" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option value="1">Oui</option>
              <option value="0">Non</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="nb_Enfant" name="nb_Enfant" autocomplete="off" required>
            <label>Nombre enfants:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="cni_Recto" name="cni_Recto" autocomplete="off" required>
            <label>cni recto:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="cni_Verso" name="cni_Verso" autocomplete="off" required>
            <label>cni verso:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

        </div>


        <div class="carousel-item">

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="permis_De_Conduire_Recto" name="permis_De_Conduire_Recto" autocomplete="off" required>
            <label>Recto permis de conduire:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="permis_De_Conduire_Verso" name="permis_De_Conduire_Verso" autocomplete="off" required>
            <label>Verso permis de conduire:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="rib_perso" name="rib_perso" autocomplete="off" required>
            <label>RIB perso:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="tableau_amortissement" name="tableau_amortissement" autocomplete="off" required>
            <label>tableau_amortissement:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

        </div>


        <div class="carousel-item">

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="carte_Grise" name="carte_Grise" autocomplete="off" required>
            <label>carte grise:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="condition_Particuliere" name="condition_Particuliere" autocomplete="off" required>
            <label>Condition Particuliere:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control entréeAssuré2" id="attestion_SS" name="attestion_SS" autocomplete="off" required>
            <label>attestion securité sociale:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

        </div>

        <div class="carousel-item">
          <h2>Recapitulatif</h2>
          <div id="listeInfosAssuré2"> </div>


        </div>



      </div>
      <button class="carousel-control-prev border rouge updateRecapitulatifAssuré2" type="button" data-bs-target="#carouselAssuré2" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next border rouge updateRecapitulatifAssuré2" type="button" data-bs-target="#carouselAssuré2" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>

    </div>









    <div id="carouselEnfant" class="carousel slide ligneCaché" data-bs-ride="carousel">
      <div class="carousel-inner" id="carouselEnfantInner">
        <h2>Enfants</h2>

        <div class="carousel-item active">
          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEnfant" id="nom" name="nom_Enfant" autocomplete="off" required>
            <label>Nom:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEnfant" id="prenom" name="prenom_Enfant" autocomplete="off" required>
            <label>Prénom:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3">
            <label>Sexe:</label>
            <select id="Sexe" class="selectpicker form-select infosEnfant" name="sexe_Enfant" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option value="0">Homme</option>
              <option value="1">Femme</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>

        </div>
        <div class="carousel-item">
          <div class="form-floating mb-3">
            <input type="text" class="form-control infosEnfant" id="Date_de_naissance" name="date_Naissance_Enfant" autocomplete="off" required>
            <label>Date de naissance:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="form-floating mb-3">
            <input type="text" class="form-control infosEnfant" id="Numero_de_securite_sociale" name="N_SS_Enfant" autocomplete="off" required>
            <label>Numéro de securité sociale:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <button type="button" class="btn btn-outline-primary" id="addEnfant">Ajouter une page enfant</button>
        </div>

        <div class="carousel-item">

          <h2>Recapitulatif</h2>
          <div id="choisirEnfantAffiché"> </div>
          <div id="infosEnfants"> </div>

          <div class="form-floating mb-3">
            <input type="text" class="form-control " id="inputNuméro" autocomplete="off">
            <label>Numéro enfant : </label>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>



          <div class="mb-3">
            <label>Informations à modifier : </label>
            <select class="selectpicker form-select " id="trucAModif">
              <option value="" disabled selected>Cliquez ici</option>
              <option>nom</option>
              <option>prenom</option>
              <option>Sexe</option>
              <option>Date_de_naissance</option>
              <option>Numero_de_securite_sociale</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="form-floating mb-3">
            <input type="text" class="form-control " id="valueModif" autocomplete="off">
            <label>Nouvelle valeur : </label>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>



          <button type="button" class="btn btn-outline-primary" id="updateEnfant">Update</button>

        </div>

      </div>



      <button class="carousel-control-prev border rouge updateRecapitulatif " type="button" data-bs-target="#carouselEnfant" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next border rouge updateRecapitulatif " type="button" data-bs-target="#carouselEnfant" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>


    </div>













    <div id="carouselEntreprise" class="carousel slide ligneCaché" data-bs-ride="carousel">
      <div class="carousel-inner" id="carouselEntrepriseInner">
        <h2>Entreprise</h2>
        <div class="carousel-item active">
          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="Raison Sociale" name="raison_Sociale" autocomplete="off" required>
            <label>Raison Sociale:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="Activité" name="activite" autocomplete="off" required>
            <label>Activité:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="Adresse" name="adresse" autocomplete="off" required>
            <label>Adresse:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="SIRET" name="siret" autocomplete="off" required>
            <label>SIRET:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>
        </div>

        <div class="carousel-item">



          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="Effectif" name="effectif" autocomplete="off" required>
            <label>Effectif:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="Date de Création" name="date_De_Creation" autocomplete="off" required>
            <label>Date de Création:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="Code NAF" name="code_naf" autocomplete="off" required>
            <label>Code NAF:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="Chiffre d’affaire" name="chiffre_Affaire" autocomplete="off" required>
            <label>Chiffre d’affaire:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


        </div>

        <div class="carousel-item">


          <div class="mb-3">
            <label>Forme Juridique:</label>
            <select id="Forme Juridique" class="selectpicker form-select infosEntreprise" name="forme_Juridique" required>
              <option value="" disabled selected>Cliquez ici</option>
              <option>Auto-entrepreneur</option>
              <option>EI</option>
              <option>EIRL</option>
              <option>EURL</option>
              <option>MICRO</option>
              <option>PL</option>
              <option>SA</option>
              <option>SARL</option>
              <option>SAS</option>
              <option>SASU</option>
              <option>SCA</option>
              <option>SCi</option>
              <option>SCS</option>
              <option>SNC</option>
            </select>
            <div class="invalid-feedback">Non rempli</div>
            <div class="valid-feedback">Rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="CCN" name="ccn" autocomplete="off" required>
            <label>CCN:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>



          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="kbis" name="kbis" autocomplete="off" required>
            <label>kbis:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="cni_Recto" name="cni_Recto" autocomplete="off" required>
            <label>Carte d'identité Recto:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>
        </div>

        <div class="carousel-item">

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="cni_Verso" name="cni_Verso" autocomplete="off" required>
            <label>Carte d'identité Verso:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="rib_pro" name="rib_pro" autocomplete="off" required>
            <label>Rib pro:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>



          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="bilan_N1" name="bilan_N1" autocomplete="off" required>
            <label>bilan N1:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="carte_Grise" name="carte_Grise" autocomplete="off" required>
            <label>carte_Grise:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>
        </div>
        <div class="carousel-item">

          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="bail" name="bail" autocomplete="off" required>
            <label>bail:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>



          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="q4" name="q4" autocomplete="off" required>
            <label>q4:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="q18" name="q18" autocomplete="off" required>
            <label>q18:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="q19" name="q19" autocomplete="off" required>
            <label>q19:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


        </div>
        <div class="carousel-item">


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="condition_Particuliere" name="condition_Particuliere" autocomplete="off" required>
            <label>condition_Particuliere:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>


          <div class="mb-3 mt-3 form-floating">
            <input type="text" class="form-control infosEntreprise" id="attestation_SS" name="attestation_SS" autocomplete="off" required>
            <label>attestation_SS:</label>
            <div class="valid-feedback">Rempli</div>
            <div class="invalid-feedback">Non rempli</div>
          </div>
        </div>


        <div class="carousel-item">


          <h2>Recapitulatif</h2>
          <div id="listeInfosEntreprise"> </div>

        </div>

      </div>




      <button class="carousel-control-prev border rouge updateRecapitulatifEntreprise " type="button" data-bs-target="#carouselEntreprise" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next border rouge updateRecapitulatifEntreprise   " type="button" data-bs-target="#carouselEntreprise" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>


    </div>



    <div id="envoiJson"><button type="button" class="btn btn-outline-light" id="testjson">Envoyer le formulaire</button></div>


  </form>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <script src="carouselNotScrolling.js"></script>
  <script src="ficheGenerale.js"></script>




</body>

</html>