<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <link rel="icon" type="image/png" href="../documents/favicon.png" />
  <title>Formulaire Particulier</title>
</head>
<header>
  <?php include 'header.php'; ?>
</header>

<body>
  <h2>FORMULAIRE PARTICULIER</h2>
  <div class="formulaire-general-Pc">
    <form action="../control/controlAssuré.php" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="col">
          <label for="last-name" class="form-label ">Nom:</label>
          <input type="text" class="form-control" id="last-name" name="last-name" autocomplete="off" required>
        </div>
        <div class="col">
          <label for="first-name" class="form-label ">Prénom:</label>
          <input type="text" class="form-control" id="first-name" name="first-name" autocomplete="off" required>
        </div>

        <div class="col">
          <label for="DN">Date de naissance:</label>
          <input type="date" pattern="(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)\d\d" class="form-control" id="DN" name="DN" autocomplete="off" required>
        </div>
        <div class="col">
          <label for="LN">Lieu de naissance:</label>
          <input type="text" class="form-control" id="LN" name="LN" autocomplete="off" required>
        </div>

      </div>


      <div class="row">
        <div class="col">
          <label for="adresse">Adresse:</label>
          <input type="text" class="form-control" id="adresse" name="adresse" autocomplete="off" required>
        </div>
        <div class="col">
          <label for="mobile">Mobile:</label>
          <input type="text" pattern="(01|02|03|04|05|06|07|08|09)[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" class="form-control" id="mobile" name="mobile" autocomplete="off" required>
        </div>
      </div>


      <div class="row">
        <div class="col">
          <label for="tel_bureau">Tel Bureau:</label>
          <input type="text" pattern="(01|02|03|04|05|06|07|08|09)[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" class="form-control" id="tel_bureau" name="tel_bureau" autocomplete="off" required>
        </div>

        <div class="col">
          <label for="email">Email:</label>
          <input type="text" pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})" class="form-control" id="email" name="email" autocomplete="off" required>
        </div>
      </div>

      <div class="row">

        <div class="col">
          <label for="profession">Profession:</label>
          <input type="text" class="form-control" id="profession" name="profession" autocomplete="off" required>
        </div>

        <div class="col">
          <label for="N°SS">Numéro de securité sociale:</label>
          <input type="text" class="form-control" id="N°SS" name="N°SS" autocomplete="off" required>
        </div>


      </div>

      <div class="row">
        <div class="col">
          <label class="form-label">Fumeur:</label>
          <select pattern="true|false|0|1" class="selectpicker form-select form-select-sm" name="fumeur" required>
            <option value="" disabled selected></option>
            <option value='1'>Oui</option>
            <option value='0'>Non</option>
          </select>
        </div>
        <div class="col">
          <label class="form-label">Statut:</label>
          <select class="selectpicker form-select form-select-sm" name="status" required>
            <option value="" disabled selected></option>
            <option>Salarié</option>
            <option>Sans emploi</option>
            <option>Chef d'entreprise TNS</option>
            <option>Mandataire social</option>
            <option>Agriculteur/éleveur</option>
            <option>Retraité SS</option>
            <option>Retraité TNS</option>
            <option>Etudiant</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label class="form-label">Caisse Maladie/Retraite:</label>
          <select class="selectpicker form-select form-select-sm" name="C-M/R" required>
            <option value="" disabled selected></option>
            <option>SS</option>
            <option>SSI</option>
            <option>PL</option>
            <option>MSA</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label class="form-label">Situation Familiale:</label>
          <select class="selectpicker form-select form-select-sm" name="S-F" required>
            <option value="" disabled selected></option>
            <option>Célibataire</option>
            <option>Union libre / Concubin</option>
            <option>Marié(e)</option>
            <option>Divorcé(e)</option>
            <option>Séparé(e)</option>
            <option>Veuf</option>
            <option>Pacsé(e)</option>
          </select>
        </div>
        <div class="col">
          <label for="chef" class="form-label">Si chef d'entreprise</label>
          <input class="form-control" list="browsers" name="chef" id="browser">
          <datalist id="browsers">
            <option value="Nom Propre">
            <option value="Gérant Majoritaire">
            <option value="Mandataire social">
            <option value="Auto entrepreneur">
            <option value="Profession libérale">
          </datalist>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label class="form-label">Sexe:</label>
          <select pattern="true|false|0|1" class="selectpicker form-select form-select-sm" name="sexe" required>
            <option value="" disabled selected></option>
            <option value="0">Homme</option>
            <option value="1">Femme</option>
          </select>
        </div>
        <div class="col">
          <label class="form-label">Parent:</label>
          <select pattern="true|false|0|1" class="selectpicker form-select form-select-sm" name="parent" required>
            <option value="" disabled selected></option>
            <option value="1">Oui</option>
            <option value="0">Non</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Nbre d'Enfant(s)</label>
          <input type="number" class="form-control" name="nb_Enfant" min="0" autocomplete="off" value="0">
        </div>
        <div class="col">
          <label>CNI Recto</label>
          <input type="file" class="form-control" name="cni_Recto" autocomplete="off">
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>CNI Verso</label>
          <input type="file" class="form-control" name="cni_Verso" autocomplete="off">
        </div>
        <div class="col">
          <label>Permis de Conduire Recto</label>
          <input type="file" class="form-control" name="permis_De_Conduire_Recto" autocomplete="off">
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Permis de Conduire Verso</label>
          <input type="file" class="form-control" name="permis_De_Conduire_Verso" autocomplete="off">
        </div>
        <div class="col">
          <label>RIB</label>
          <input type="file" class="form-control" name="rib_perso" autocomplete="off">
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Tableau d'Amortissement</label>
          <input type="file" class="form-control" name="tableau_amortissement" autocomplete="off">
        </div>
        <div class="col">
          <label>Carte Grise</label>
          <input type="file" class="form-control" name="carte_Grise" autocomplete="off">
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label>Conditions Particulières</label>
          <input type="file" class="form-control" name="condition_Particuliere" autocomplete="off">
        </div>
        <div class="col">
          <label>Attestation SS</label>
          <input type="file" class="form-control" name="attestion_SS" autocomplete="off">
        </div>
      </div>
      <hr>
      <div class="row">
        <input type="submit" value="Envoyer" class="form-control">
      </div>
    </form>
  </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

</html>