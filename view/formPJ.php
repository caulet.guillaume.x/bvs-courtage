<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="styles.css">
    <link rel="icon" type="image/png" href="../documents/favicon.png" />
    <title>Formulaire PJ</title>
</head>
<header>
    <?php include 'header.php'; ?>
</header>

<body>
    <h2>RECUEIL DES BESOINS PROTECTION JURIDIQUE PRO/PART</h2>
    <div class="formulaire-general-Pc">
        <form action="../control/controlPJ.php" method="post" enctype="multipart/form-data"></form>

        <div class="row">
            <!-- ligne 1 -->

            <div class="col">
                <!-- colonne 1 -->
                <label>Date Recueil</label>
                <input class="form-control" pattern="(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)\d\d" name="date_Recueil" type="text" autocomplete="off">
            </div>
            <div class="col">
                <!-- colonne 2 -->
                <label>Remis Par</label>
                <input class="form-control" name="remis_Par" type="text" autocomplete="off">
            </div>

        </div>

        <div class="row">
            <!-- ligne 2 -->

            <div class="col">
                <!-- colonne 1 -->
                <label>Nombre de Salaries</label>
                <input class="form-control" pattern="[0-9]+" name="nbre_Salaries" type="number" autocomplete="off">
            </div>
            <div class="col">
                <!-- colonne 2 -->
                <label>Nombre de Vehicules</label>
                <input class="form-control" pattern="[0-9]+" name="nbre_Vehicules" type="number" autocomplete="off">
            </div>

        </div>

        <div class="row">
            <!-- ligne 3 -->

            <div class="col">
                <!-- colonne 1 -->
                <label class="form-label">Forme Juridique</label>
                <input class="form-control" name="forme_Juridique" type="text" autocomplete="off">
            </div>
            <div class="col">
                <!-- colonne 2 -->
                <label class="form-label">Avez vous fait l'objet de sinistre au cours des 5 dernières années?</label>
                <input class="form-control" pattern="[0-9]+" name="nbre_Sinistre_5_Ans" type="number" autocomplete="off">
            </div>

        </div>

        <div class="row">
            <!-- ligne 4 -->
            <div class="col">
                <!-- colonne 2 -->
                <label class="form-label">Resilation d'un précédent assureur?</label>
                <select id="mySelect" pattern="true|false|0|1" class="selectpicker form-select form-select-sm" name="resilation_Ass_Pre" required>
                    <option value="0">non</option>
                    <option value="1">oui</option>
            </div>
        </div>

        <div class="row">
            <!-- ligne 5 -->

            <div class="col">
                <!-- colonne 1 -->
                <label class="form-label">Si Oui Motif</label>
                <input class="form-control motif" name="si_Oui_Motif" placeholder="Si oui, motif" type="text" autocomplete="off">
            </div>
            <div class="col">
                <!-- colonne 2 -->
                <label class="form-label">Assureur Actuel</label>
                <input class="form-control" name="assureur_Actuel" type="text" autocomplete="off">
            </div>

        </div>

        <div class="row">
            <!-- ligne 6 -->

            <div class="col">
                <!-- colonne 1 -->
                <label class="form-label">Date Echéance</label>
                <input class="form-control" pattern="(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)\d\d" name="date_Echeance" type="text" autocomplete="off">
            </div>
            <div class="col">
                <!-- colonne 2 -->
                <label class="form-label">Commentaires</label>
                <textarea class="form-control" name="commentaires" type="text"></textarea>
            </div>

        </div>
        <div class="row">
            <div class="col">
                <a href="#"><button class="btn" type="submit">Envoyer</button></a>
            </div>
        </div>
    </div>

    </form>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

</html>