<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="../documents/favicon.png" />
    <title>Fiche entreprise</title>
</head>
<header>
    <?php include 'header.php' ?>
</header>
<body>
    <div class="grid container">
        <h2>Liste des recueils</h2>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins MRP</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins Auto</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins MRH</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins Protection Juridique Pro/particulier</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins PER/ Assurance Vie</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins Prévoyance</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins Mutuelle</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins Assurance de Prêt</div>
        </a>
        <hr>
        <a class="recueil" href="recueil.php">
            <div class="col-md-12">Recueil des besoins KDC/Dépendance/Obsèque/Garantie/Associe/Homme Clé/GAV</div>
        </a>
        <hr>
    </div>
</body>
</html>