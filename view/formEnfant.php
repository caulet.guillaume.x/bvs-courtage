<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../formPC.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="../documents/favicon.png" />
    <link rel="stylesheet" href="styles.css">

    <title>Formulaire Enfant</title>
</head>
<header>
    <?php include 'header.php'; ?>
</header>

<body>

    <div class="formulaire-general-Pc">
        <h3>Ajouter un enfant</h3>
        <form action="../control/controlEnfant.php" method="post">

            <div class="row">
                <div class="col">
                    <label>Nom:</label>
                    <input type="text" name="nom_Enfant" class="form-control" autocomplete="off">
                </div>
                <div class="col">
                    <label>Prénom:</label>
                    <input type="text" name="prenom_Enfant" class="form-control" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label>Date de naissance:</label>
                    <input type="date" pattern="(0[1-9]|[12][0-9]|3[01])[\/](0[1-9]|1[012])[\/](19|20)\d\d" name="date_Naissance_Enfant" class="form-control" autocomplete="off">
                </div>
                <div class="col">
                    <label>N°Sécurité Sociale:</label>
                    <input type="text" name="N_SS_Enfant" class="form-control" autocomplete="off">
                </div>

                <div class="row">
                    <div class="col">
                        <label>Sexe:</label>
                        <select id="mySelect" pattern="true|false|0|1" class="selectpicker form-select form-select-sm" name="sexe_Enfant" required>
                            <option value="0">Fille</option>
                            <option value="1">Garçon</option>
                    </div>
                    <div class="col">
                        <input class="btn" type="submit" value="envoyer">
                        <!--bouton disparu -->
                    </div>
                </div>
                <div class="row">

                </div>


        </form>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

</html>