<?php include '../model/data.php'; ?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="styles.css">
  <link rel="icon" type="image/png" href="../documents/favicon.png" />
  <title>Liste Assuré</title>
</head>
<?php include 'header.php' ?>
<body>
  <div class="liste">
    <a href="ficheGenerale.php" class="btn ">Formulaire</a>
    <div class="tableaux">
      <table class="table table-hover table-striped table-bordered">
      <h3>Liste des Assurés</h3>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nom & Prenom</th>
            <th scope="col">Sexe</th>
            <th scope="col">Telephone</th>
            <th scope="col">Email</th>
            <th scope="col">Date de Naissance</th>
            <th scope="col">N° Sécurité Sociale</th>
            <th scope="col">Situation Familiale</th>
            <th scope="col">Statut</th>
            <th scope="col">Profession</th>
            <th scope="col">Caisse Maladie ou Retraite</th>
          </tr>
        </thead>
        <?php $arpr = 1 ?>
        <tbody>
          <?php foreach (VoirAssuré() as $select) { ?>
            <tr>
              <th scope="row" class="th-tbody"><?php echo $select['id']; ?></th>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['nom'];?> <?php echo $select['prenom']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo boolSexe($select['sexe']); ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['mobile']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['mail']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['date_De_Naissance']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['nSS']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['situation_Familiale']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['statut']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['profession']; ?></a></td>
              <td> <a class="assure assuré<?= $arpr++ ?>" href="fiche-prospect.php?id=<?php echo $select['id'] ?>"><?php echo $select['caisse_Mal_Re']; ?></a></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
<script src=" https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</html>