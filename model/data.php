<?php
session_start();
$host = "localhost";
$db = "Oxion";
$user = "admin";
$pass = "admin";




try{
    $pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }
    catch(PDOException $e){
        echo $e->getMessage()
    ;}

function ajouterAssuré($nom,$prenom,$dateNaissance,$lieuNaissance,$adresse,$mobile,$tel,$mail,$profession,$nSS,$fumeur,$statut,$caisseMaladie,$situation,$siChef,$sexe,$parent,$nbEnfant,$cni_Recto,$cni_Verso,$permi_conduire_Recto,$permi_conduire_Verso,$rib,$tb_amortissement,$carte_grise,$condition_perticuliere,$attestation_ss){
    global $pdo;
    try{$req = $pdo->prepare('INSERT INTO Particulier(nom,prenom,date_de_Naissance,lieu_de_Naissance,adresse,mobile,tel_Bureau,mail,profession,nSS,fumeur,statut,caisse_Mal_Re,situation_Familiale,chef_Entreprise,sexe,parent,nb_Enfant,cni_Recto,cni_Verso,permis_De_Conduire_Recto,permis_De_Conduire_Verso,rib_perso,tableau_amortissement,carte_Grise,condition_Particuliere,attestion_SS)VALUES(
        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
    )');
    $req->execute([$nom,$prenom,$dateNaissance,$lieuNaissance,$adresse,$mobile,$tel,$mail,$profession,$nSS,$fumeur,$statut,$caisseMaladie,$situation,$siChef,$sexe,$parent,$nbEnfant,$cni_Recto,$cni_Verso,$permi_conduire_Recto,$permi_conduire_Verso,$rib,$tb_amortissement,$carte_grise,$condition_perticuliere,$attestation_ss]);
    return $pdo->lastInsertId();
}catch(Exception $e){
    // en cas d'erreur :
     echo " Erreur ! ".$e->getMessage();
     echo $req;

  }}

function VoirAssuré(){
    global $pdo;
    try{$req = $pdo->query('SELECT * from Particulier');
    return $req->fetchAll();
}catch(Exception $e){
    // en cas d'erreur :
     echo " Erreur ! ".$e->getMessage();
     echo $req;

  }}
  function VoirEntreprise(){
    global $pdo;
    try{$req = $pdo->query('SELECT * from Professionel');
    return $req->fetchAll();
}catch(Exception $e){
    // en cas d'erreur :
     echo " Erreur ! ".$e->getMessage();
     echo $req;

  }}

function ajouterPro($raison_sociale,$adresse,$date_de_creation,$siret,$effectif,$code_naf,$chiffre_daffaire,$activiter,$forme_juridique,$ccn,$kbis,$cni_recto,$cni_verso,$rib,$bilan_n1,$carte_grise,$bail,$q4,$q18,$q19,$condition_particuliere,$attestation_ss){
    global $pdo;
    try{$req = $pdo->prepare('INSERT INTO Professionel(
        raison_Sociale,
        adresse,
        date_De_Creation,
        siret,
        effectif,
        code_naf,
        chiffre_Affaire,
        activite,
        forme_Juridique,
        ccn,
        kbis,
        cni_Recto,
        cni_Verso,
        rib_pro,
        bilan_N1,
        carte_Grise,
        bail,
        q4,
        q18,
        q19,
        condition_Particuliere,
        attestation_SS
    )
    VALUES(
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?
        
    )');
    $req->execute([$raison_sociale,$adresse,$date_de_creation,$siret,$effectif,$code_naf,$chiffre_daffaire,$activiter,$forme_juridique,$ccn,$kbis,$cni_recto,$cni_verso,$rib,$bilan_n1,$carte_grise,$bail,$q4,$q18,$q19,$condition_particuliere,$attestation_ss]);
}catch(Exception $e){
    // en cas d'erreur :
    echo " Erreur ! ".$e->getMessage();
    echo $req;
}}

function ajouterEnfant($nom,$prenom,$date_Naissance,$nSS,$sexe){
    global $pdo;
    try{
        $req = $pdo->prepare('INSERT INTO Enfant(nom,prenom,date_Naissance,N_SS,sexe)
    VALUES(?,?,?,?,?)'
    );
    $req->execute([$nom,$prenom,$date_Naissance,$nSS,$sexe]);
    return $pdo->lastInsertId();
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}

function ajouterPJ($date_Recueil,$remis_Par,$nbre_Salaries,$nbre_Vehicules,$forme_Juridique,$nbre_Sinistre_5_Ans,$chiffre_daffaire,$si_Oui_Motif,$assureur_Actuel,$date_Echeance,$commentaires){
    global $pdo;
    try{
        $req = $pdo->prepare('INSERT INTO PJ_PRO_PART(
            date_Recueil,
            remis_Par,
            nbre_Salaries,
            nbre_Vehicules,
            forme_Juridique,
            nbre_Sinistre_5_Ans,
            resilation_Ass_Pre,
            si_Oui_Motif,
            assureur_Actuel,
            date_Echeance,
            commentaires
        )
        VALUES(
            ?,?,?,?,?,?,?,?,?,?,?
        )');
        $req->execute([$date_Recueil,$remis_Par,$nbre_Salaries,$nbre_Vehicules,$forme_Juridique,$nbre_Sinistre_5_Ans,$chiffre_daffaire,$si_Oui_Motif,$assureur_Actuel,$date_Echeance,$commentaires]);
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}

    function lierP_E($idP,$idE){
        global $pdo;
        try{
            $req = $pdo->prepare('INSERT INTO Parents_Enfants(id_Parents,id_Enfants)VALUES(?,?)');
            $req->execute([$idP,$idE]);
        }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
        }
    }


function boolSexe ($sexe)
{if ($sexe == 0 ){
    return 'Homme';
}else{
    return 'Femme';
}
}
function boolParent ($parent)
{if ($parent == 0 ){
    return 'Non';
}else{
    return 'Oui';
}
}

function boolFumeur ($fumeur)
{if ($fumeur == 0 ){
    return 'Non';
}else{
    return 'Oui';
}
}
function voir1Assure($id){
    global $pdo;
    try{
        $req = $pdo->prepare('SELECT * FROM Particulier WHERE id = ?');
        $req->execute([$id]);
        return $req->fetchAll();
}catch(Exception $e){
    // en cas d'erreur :
    echo " Erreur ! ".$e->getMessage();
    echo $req;
}
}

function voir1Entreprise($id){
    global $pdo;
    try{
        $req = $pdo->prepare('SELECT * FROM Professionel WHERE id = ?');
        $req->execute([$id]);
        return $req->fetchAll();
}catch(Exception $e){
    // en cas d'erreur :
    echo " Erreur ! ".$e->getMessage();
    echo $req;
}
}

function uploadFiles($champ){
    move_uploaded_file($_FILES[$champ]['tmp_name'], '../control/upload/'.$_FILES[$champ]['name']);
    return 'upload/'.$_FILES[$champ]['name'];
}

function lierE_A($idE,$idA){
    global $pdo;
    try{
        $req = $pdo->prepare('INSERT INTO Entreprise_Assure(id_Entreprise,id_Assure)VALUES(?,?)');
        $req->execute([$idE,$idA]);
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}

function voirTBlaison(){
    global $pdo;
    try{
        $req = $pdo->query('SELECT * from Entreprise_Assure');
        return $req->fetchAll();
    }catch(Exception $e){
        // en cas d'erreur :
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    }
}
?>