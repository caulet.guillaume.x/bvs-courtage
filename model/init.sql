DROP DATABASE IF EXISTS Oxion;
CREATE DATABASE Oxion;

USE Oxion;
GRANT ALL PRIVILEGES ON Oxion.* TO 'admin'@'localhost';

CREATE TABLE Particulier(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(256),
    prenom VARCHAR(256),
    date_De_Naissance VARCHAR(256),
    lieu_De_Naissance VARCHAR(256),
    adresse VARCHAR(256),
    mobile INT,
    tel_Bureau INT,
    mail VARCHAR(256),
    profession VARCHAR (256),
    nSS VARCHAR(256),
    fumeur BOOLEAN,
    statut VARCHAR(256),
    caisse_Mal_Re VARCHAR(256),
    situation_Familiale VARCHAR(256),
    chef_Entreprise VARCHAR(256),
    sexe BOOLEAN,
    parent BOOLEAN,
    nb_Enfant INT,
    cni_Recto VARCHAR(256),
    cni_Verso VARCHAR(256),
    permis_De_Conduire_Recto VARCHAR(256),
    permis_De_Conduire_Verso VARCHAR(256),
    rib_perso VARCHAR(256),
    tableau_amortissement VARCHAR(256),
    carte_Grise VARCHAR (256),
    condition_Particuliere VARCHAR (256),
    attestion_SS VARCHAR(256)
);

CREATE TABLE Professionel (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
raison_Sociale VARCHAR(256),
adresse VARCHAR (256),
date_De_Creation VARCHAR(256),
siret INT,
effectif INT,
code_naf VARCHAR (256),
chiffre_Affaire INT,
activite VARCHAR(256),
forme_Juridique VARCHAR (256),
ccn VARCHAR (256),
kbis VARCHAR (256),
cni_Recto VARCHAR(256),
cni_Verso VARCHAR(256),
rib_pro VARCHAR(256),
bilan_N1 VARCHAR(256),
carte_Grise VARCHAR(256),
bail VARCHAR(256),
q4 VARCHAR(256),
q18 VARCHAR(256),
q19 VARCHAR(256),
condition_Particuliere VARCHAR(256),
attestation_SS VARCHAR(256)
);
CREATE TABLE Enfant (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nom VARCHAR(256),
prenom VARCHAR (256),
date_Naissance VARCHAR(256),
N_SS VARCHAR(256),
sexe BOOLEAN
);

CREATE TABLE PJ_PRO_PART(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    date_Recueil VARCHAR(256),
    remis_Par VARCHAR (256),
    nbre_Salaries INT,
    nbre_Vehicules INT,
    forme_Juridique VARCHAR (256),
    nbre_Sinistre_5_Ans INT,
    resilation_Ass_Pre BOOLEAN,
    si_Oui_Motif VARCHAR (256),
    assureur_Actuel VARCHAR (256),
    date_Echeance VARCHAR(256),
    commentaires VARCHAR (256)
);
CREATE TABLE Parents_Enfants (
    id_Parents INT UNSIGNED,
    id_Enfants INT UNSIGNED,
    UNIQUE (id_Parents,id_Enfants),
    FOREIGN KEY (id_Parents) REFERENCES Particulier(id),
    FOREIGN KEY (id_Enfants) REFERENCES Enfant(id)
);

CREATE TABLE Entreprise_Assure (
    id_Entreprise INT UNSIGNED,
    id_Assure INT UNSIGNED,
    UNIQUE (id_Entreprise,id_Assure),
    FOREIGN KEY (id_Entreprise) REFERENCES Professionel(id),
    FOREIGN KEY (id_Assure) REFERENCES Particulier(id)
);